---
title: "Say hello to k8s.x33u"
description: "Introducing Doks, a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default."
excerpt: "Introducing Doks, a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default."
date: 2020-11-04T09:19:42+01:00
lastmod: 2020-11-04T09:19:42+01:00
draft: false
weight: 50
images: ["brigles.jpg"]
categories: ["News"]
tags: ["hello world", "kubernetes", "blog"]
contributors: ["Gordon Lenz"]
pinned: false
homepage: false
---

Introducing Doks, a Hugo theme helping you build modern documentation websites that are secure, fast, and SEO-ready — by default.
