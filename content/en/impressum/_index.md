---
title : "Impressum"
description: "Impressumsdetails"
lead: ""
date: 2020-10-06T08:48:45+00:00
lastmod: 2020-10-06T08:48:45+00:00
draft: false
images: []
weight: 100
---

#### Informationen gem. § 5 Telemediengesetz und § 55 Abs. 1 RStV


##### Das Internet-Angebot wird bereitgestellt durch:

Gordon Lenz <br>
Westring 7 <br>
39108 Magdeburg <br>
Germany <br><br>
Telefon: <font color="#68696f"> +49 (0) 391 / 59 848 746</font><br>
Fax: <font color="#68696f"> +49 (0) 391 / 59 848 773</font><br>
E-Mailadresse: <font color="#68696f">golenz(at)x33u.org</font> <br>
PGP-Key: <font color="#68696f" text-decoration="none">[0x992B9A25E7EF0D4D](https://www.x33u.org/dwnld/golenz_pub.asc)</font> <br>
Fingerprint: <font color="#68696f">
F3A4 F552 9F2B 04E3 5082 A0C8 992B 9A25 E7EF 0D4D
</font> <br>

#### Verantwortlich i.S.d. § 55 Abs. 2 Rundfunkstaatsvertrag:

Gordon Lenz <br>
Westring 7 <br>
39108 Magdeburg <br>
Germany <br>
<br>

# Disclaimer

### Rechtliche Hinweise

Bitte beachten Sie die wichtigen rechtlichen Hinweise zu den Inhalten und zur Verfügbarkeit dieser Webseiten, zum Urheberrecht und zu externen Links.

### Inhalte dieser Website

Die Inhalte dieser Website werden mit größtmöglicher Sorgfalt erstellt. Der Betreiber dieser Website übernimmt jedoch keine Gewähr für die Richtigkeit, Vollständigkeit und Aktualität der bereitgestellten Inhalte.

### Verfügbarkeit der Website

Der Betreiber dieser Website wird sich bemühen, den Dienst möglichst unterbrechungsfrei zum Abruf anzubieten. Auch bei aller Sorgfalt können aber Ausfallzeiten nicht ausgeschlossen werden. Sie behält sich das Recht vor, ihr Angebot jederzeit zu ändern oder einzustellen. Für durch nicht fehlerfrei angelegte Dateien oder nicht fehlerfrei strukturierte Formate bedingte Unterbrechungen oder anderweitige Störungen können wir keine Gewähr übernehmen.

### Urheberrecht

Alle Inhalte und Strukturen dieser Website sind urheber- und leistungsschutzrechtlich geschützt. Die Veröffentlichung im World Wide Web oder in sonstigen Diensten des Internet bedeutet noch keine Einverständniserklärung für eine anderweitige Nutzung durch Dritte. Jede vom deutschen Urheberrecht nicht zugelassene Verwertung bedarf der vorherigen schriftlichen Zustimmung der Berliner Beauftragten für Datenschutz und Informationsfreiheit.

Wir erlauben und begrüßen ausdrücklich das Zitieren unserer Dokumente sowie das Setzen von Links auf unsere Website, solange kenntlich gemacht wird, dass es sich um Inhalte der Website der Berliner Beauftragten für Datenschutz und Informationsfreiheit handelt und diese Inhalte nicht in Verbindung mit Inhalten Dritter gebracht werden, die den Interessen der Berliner Beauftragten für Datenschutz und Informationsfreiheit widersprechen.

### Hinweis zur Problematik von externen Links

Der Betreiber dieser Website ist als Inhaltsanbieter nach § 7 Abs.1 Telemediengesetz für die “eigenen Inhalte”, die er zur Nutzung bereithält, nach den allgemeinen Gesetzen verantwortlich. Von diesen eigenen Inhalten sind Querverweise (“Links”) auf die von anderen Anbietern bereitgehaltenen Inhalte zu unterscheiden. Durch den Querverweis hält Der Betreiber dieser Website insofern “fremde Inhalte” zur Nutzung bereit, die in dieser Weise gekennzeichnet sind:

Bei “Links” handelt es sich stets um “lebende” (dynamische) Verweisungen. Der Betreiber dieser Website hat bei der erstmaligen Verknüpfung zwar den fremden Inhalt daraufhin überprüft, ob durch ihn eine mögliche zivilrechtliche oder strafrechtliche Verantwortlichkeit ausgelöst wird. Sie überprüft aber die Inhalte, auf die sie in ihrem Angebot verweist, nicht ständig auf Veränderungen, die eine Verantwortlichkeit neu begründen könnten. Wenn sie feststellt oder von anderen darauf hingewiesen wird, dass ein konkretes Angebot, zu dem sie einen Link bereitgestellt hat, eine zivil- oder strafrechtliche Verantwortlichkeit auslöst, wird sie den Verweis auf dieses Angebot aufheben.
