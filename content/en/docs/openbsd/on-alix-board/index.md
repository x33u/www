---
title: "OpenBSD on Alix Board"
description: "a howto for installing OpenBSD on the PC Engines Alix Board"
lead: "a howto for installing OpenBSD on the PC Engines Alix Board"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---

> if you wouldnt spend your time with hours of waiting after exec some command please use at least more than 250M of `swap` cause kernel relinking only need at least about 180M of swap

#### _files we need:_

* bsd.rd (i386)
* pxeboot
* install.conf
* dnsmasq
* nullmodem
* USB NIC _optional_

```sh
## setup "/etc/dnsmasq.conf"
port=0
interface=enp0s20u1 # change me
bind-interfaces
dhcp-range=10.1.0.100,10.1.0.254,12h
dhcp-boot=/tftpboot/pxeboot
enable-tftp
tftp-root=/tftpboot/

## add ip to USB NIC
> ip addr add 10.1.0.10/24 dev enp0s20u1

## cp files to tftpboot
> cp {install.conf,bsd.rd,pxeboot} /tftpboot/

## make tftp dir
> mkdir /tftpboot/etc

## edit /tftpboot/etc/boot.conf
stty com0 9600
set tty com0
boot tftp:/bsd.rd

## setup /tftpboot/install.conf
System hostname = server1
Password for root = $2b$14$Z4xRMg8vDpgYH...GVot3ySoj8yby
Change the default console to com0 = yes
Which speed should com0 use = 9600
Setup a user = puffy
Password for user = *************
Public ssh key for user = ssh-ed25519 AAAAC3NzaC1...g3Aqre puffy@ai
What timezone are you in = Europe/Stockholm
Location of sets = http
HTTP Server = cdn.openbsd.org

## start dnsmasq
> systemctl start dnsmasq.service

## watch service
> journalctl -u dnsmasq.service -f
```

> use port `vr0` for tftp (usb side)

add swap per usb
```sh
## find usb disk
> sysctl hw.disknames

## edit partitions
> disklabel -E sd0 # 1087666 is about 512M

## edit /etc/fstab
999999a8942e228b.a none swap sw

## read swap from fstab and start
> swapctl	-A
```

> For alix boards, the default setting is 38400 8N1, no flow control

see [handbook](https://www.pcengines.ch/pdf/alix2.pdf)
```sh
## To enter setup, type S during the memory test. You should see something like the following:
PC Engines ALIX.2 v0.98j
640 KB Base Memory
261120 KB Extended Memory
01F0 Master 848A CF 128MB
Phys C/H/S 1002/8/32 Log C/H/S 1002/8/32
BIOS setup:
(9) 9600 baud (2) 19200 baud *3* 38400 baud (5) 57600 baud (1) 115200 baud
*C* CHS mode (L) LBA mode (W) HDD wait (V) HDD slave (U) UDMA enable
(M) MFGPT workaround
(P) late PCI init
*R* Serial console enable
(E) PXE boot enable
(X) Xmodem upload
(Q) Quit
```

