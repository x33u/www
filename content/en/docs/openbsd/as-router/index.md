---
title: "OpenBSD as router"
description: "this guide will show how to OpenBSD as router"
lead: "this guide will show how to OpenBSD as router"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---

_goals:_

* build a router
* dhcp
* unbound
* guast network

### forward traffic
```sh
## forward v4 and v6 traffic
> echo 'net.inet.ip.forwarding=1' >> /etc/sysctl.conf
> echo 'net.inet6.ip6.forwarding=1' >> /etc/sysctl.conf
```

### setup interfaces
```sh
## wan dhcp
> echo 'dhcp' > /etc/hostname.vr0

## lan networks
> echo 'inet 192.168.0.1 255.255.255.0 192.168.0.255' > /etc/hostname.vr1
> echo 'inet 192.168.1.1 255.255.255.0 192.168.1.255' > /etc/hostname.vr2
```

### dhcp
```sh
## configure dhcpd
> rcctl enable dhcpd
> rcctl set dhcpd flags vr1 vr2

## edit /etc/dhcpd.conf
subnet 192.168.0.0 netmask 255.255.255.0 {
	option routers 192.168.0.1;
	option domain-name-servers 192.168.0.1;
	range 192.168.0.4 192.168.0.254;
	host myserver {
		fixed-address 192.168.0.2;
		hardware ethernet 00:00:00:00:00:00;
	}
	host mylaptop {
		fixed-address 192.168.1.3;
		hardware ethernet 11:11:11:11:11:11;
	}
}
subnet 192.168.1.0 netmask 255.255.255.0 {
	option routers 192.168.1.1;
	option domain-name-servers 192.168.1.1;
	range 192.168.1.2 192.168.1.254;
}
```

### configure pf
```sh
## edit /etc/pf.conf
ext_if = "vr0"
int_if = "vr1"
dmz_if = "vr2"

lan_net = "192.168.0.0/24"
dmz_net = "192.168.1.0/24"

set block-policy drop
set loginterface egress
set skip on lo0

match in all scrub (no-df random-id max-mss 1440)
match out on egress inet from !(egress:network) to any nat-to (egress:0)

antispoof quick for { egress $int_if $dmz_if }

block all

pass out quick
pass in on $int_if

pass in on $dmz_if from $dmz_if:network to any
pass in on egress inet6 proto icmp6 all icmp6-type { routeradv neighbrsol neighbradv }
pass in on egress inet6 proto udp from fe80::/10 port dhcpv6-server to fe80::/10 port dhcpv6-client no state
pass in on egress inet6 proto icmp6 all icmp6-type echoreq
pass in on egress inet proto icmp all icmp-type echoreq

block in from $dmz_if:network to $lan_net
```

### dns
```sh
## enable service
> rcctl enable unbound

## edit vi /var/unbound/etc/unbound.conf
server:
	interface: 192.168.0.1
	interface: 192.168.1.1
	interface: 127.0.0.1
	access-control: 192.168.0.0/24 allow
	access-control: 192.168.1.0/24 allow
	do-not-query-localhost: no
	hide-identity: yes
	hide-version: yes

forward-zone:
	name: "."				# use for ALL queries
	forward-addr: 176.9.93.198		# dnsforge.de Werbeblocker
	forward-addr: 2a01:4f8:151:34aa::198	# dnsforge.de Werbeblocker
	forward-addr: 176.9.1.117		# dnsforge.de Werbeblocker
	forward-addr: 2a01:4f8:141:316d::117	# dnsforge.de Werbeblocker
	forward-addr: 46.182.19.48		# Digital Courage ev
	forward-addr: 2a02:2970:1002::18	# Digital Courage ev
	forward-addr: 80.241.218.68		# dismail Werbe- und Tracking-Filterliste
	forward-addr: 2a02:c205:3001:4558::1	# dismail Werbe- und Tracking-Filterliste
	forward-addr: 94.75.228.29		# Chaos Computer Club
	forward-addr: 64.6.64.6			# Verisign
	forward-addr: 2a02:2970:1002::18	# Chaos Computer Club
	forward-addr: 1.1.1.1			# cloudflare
	forward-addr: 8.8.8.8			# Google
	forward-first: yes			# try direct if forwarder fails

## restart service
> rcctl restart unbound
```

### slow boot
```sh
## == disable "reordering libraries" at boot time
rcctl disable library_aslr
```
> at this moment i dont know if this cmd run library_aslr later or never<br>
> to definely disable use `rc.conf` flags
