---
title: "OpenBSD chroot(8)"
description: "short guide for using chroot"
lead: "short guide for using chroot"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---


_"The chroot command changes its root directory to the supplied directory newroot and executes command, if supplied, or an interactive copy of the user's shell."_

#### _goals:_
* chroot ssh

#### _get starting:_

```sh
## change to workdir
> cd /foo/bar

## get files from cdn of trust
> wget https://cdn.openbsd.org/pub/OpenBSD/6.6/amd64/base66.tgz
> wget https://cdn.openbsd.org/pub/OpenBSD/6.6/amd64/comp66.tgz
> wget https://cdn.openbsd.org/pub/OpenBSD/6.6/amd64/man66.tgz
```

```sh
## create chroot dir
> mkdir -p /foo/bar/ssh_chroot
> cd /foo/bar/ssh_chroot

## copy system resolv.conf to chroot dir
> cp /etc/resolv.conf /foo/bar/ssh_chroot/ssh/resolv.conf

## extract packs
> tar -xvzf /foo/bar/ssh_chroot/base67.tar
> tar -xvzf /foo/bar/ssh_chroot/comp67.tar
> tar -xvzf /foo/bar/ssh_chroot/man67.tar

## MAKEDEV
> cd dev
> ./MAKEDEV all
```

```sh
## chroot in /foo/bar
> chroot /foo/bar/ssh_chroot/ ldconfig /usr/lib /usr/local/lib
> chown root:wheel /foo/bar/ssh_chroot/
> chmod 755 /foo/bar/ssh_chroot/

## setup chroot user
> groupadd ssh-users
> useradd -g ssh-users -d /chroot/ssh/home/ssh-user -m ssh-user
> passwd ssh-users
```

```sh
## to activate append chroot in "/etc/ssh/sshd_conf"
> echo "ChrootDirectory /var/chroot/ssh" >> /etc/ssh/sshd_conf
```
