---
title: "OpenBSD on BananaPi-M1"
description: "a howto for installing OpenBSD on the BananaPi-M1"
lead: "a howto for installing OpenBSD on the BananaPi-M1"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---


_"OpenBSD/armv7 is a port of OpenBSD to various ARMv7 based systems"_

#### _goals:_

* build [u-boot](https://duckduckgo.com/l/?kh=-1&uddg=https%3A%2F%2Fde.wikipedia.org%2Fwiki%2FDas_U-Boot) with docker
* run [OpenBSD](https://openbsd.org) from SATA

#### _requirements:_

* sdcard for bootloader
* SATA device for system
* [cubieboard filesystem for armv7 ](https://cdn.openbsd.org/pub/OpenBSD/6.7/armv7/miniroot-cubie-67.fs)
* [sunxi-bootloader](https://www.x33u.org/dwnld/u-boot-sunxi-with-spl.bin)

<br>

```sh
## build bootloader with docker
## after build you will find the bootloader in **/tmp/u-boot**
## see: https://hub.docker.com/r/golenz/u-boot-sunxi-bpim1
> docker run --rm -it -v /tmp/u-boot:/u-boot golenz/u-boot-sunxi-bpim1:latest \
               /bin/sh -c 'apk add python3 python3-dev \
               && git clone git://git.denx.de/u-boot.git \
               && cd u-boot \
               && make -j4 ARCH=arm CROSS_COMPILE=arm-none-eabi- Bananapi_defconfig \
               && make -j4 ARCH=arm CROSS_COMPILE=arm-none-eabi-'

## prepare SATA device
## write system to "sata" device
> dd if=miniroot-cubie-67.fs of=/dev/sdX

## write u-boot to "sdcard"
> dd if=/tmp/u-boot/u-boot-sunxi-with-spl.bin of=/dev/sdX bs=1024 seek=8

```

now put the stuff together and do the "smoke test" to see if the system cames up -
connect serial to `TX=P01` `RX=P02` `GND=P08 or J12`

> change `RX <-> TX` if there is no input on console

if there is no other USB device pluged in `u-boot` choose SATA out of the box otherwise change u-boot environment

```sh
## do not miss the boot-cmdline
> setenv bootcmd 'scsi scan; scsi dev 0; scsi part 0:1;run bootcmd_scsi0'
> saveenv # if you want make persistent
> boot

```

[check official OpenBSD on armv7 site](https://cdn.openbsd.org/pub/OpenBSD/snapshots/armv7/INSTALL.armv7)
