---
title: "OpenBSD acme-client(1)"
description: "short guide for acme-client"
lead: "short guide for acme-client"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---

_"acme-client is an Automatic Certificate Management Environment (ACME)
client: it looks in its configuration for a domain section corresponding
to the domain given as command line argument and uses that configuration
to retrieve an X.509 certificate which can be used to provide domain name
validation"_


### setup httpd
```sh
## first setup httpd well known acme location in "/etc/httpd.conf"
server "www.example.com" {
    alias "example.com"
    listen on * port 80
    root "/htdocs/www.example.com"
    location "/.well-known/acme-challenge/*" {
        root "/acme"
        request strip 2
    }
}
```

```sh
## testing "httpd.conf" file
> httpd -nf /etc/httpd.conf

## start httpd forced
> rcctl -f start httpd

## enable httpd
> rcctl enable httpd
```

```sh
## create /etc/acme-client.conf
authority letsencrypt {
	api url "https://acme-v02.api.letsencrypt.org/directory"
	account key "/etc/acme/letsencrypt-privkey.pem"
}

authority letsencrypt-staging {
	api url "https://acme-staging-v02.api.letsencrypt.org/directory"
	account key "/etc/acme/letsencrypt-staging-privkey.pem"
}

domain example.com {
	alternative names { secure.example.com }
	domain key "/etc/ssl/private/example.com.key"
	domain full chain certificate "/etc/ssl/example.com.fullchain.pem"
	sign with letsencrypt
}

```

```sh
## create acme certificate
> acme-client -v www.example.com
```

> **Note**: Certificates are default load from<br>
`/etc/ssl/private/server.key `<br>
`/etc/ssl/server.crt`<br>

to load acme certificates we need to instruct `httpd` where these stored

```apacheconf
## configure "/etc/httpd.conf"
server "www.example.com" {
    alias "example.com"
    listen on * port 80
    listen on * tls port 443
    root "/htdocs/www.example.com"
    tls certificate "/etc/ssl/acme/fullchain.pem"
    tls key "/etc/ssl/acme/private/privkey.pem"
    location "/.well-known/acme-challenge/*" {
        root "/acme"
        request strip 2
    }
    # this will redirect traffic to tls port 443
    block return 301 "https://$SERVER_NAME$REQUEST_URI"
}
```

```sh
## we need to reload config for httpd
> rcctl reload httpd
```

so we want to make shure that we route every traffic over https with `relayd`

```sh
## add to /etc/relayd.conf
relay "proxyssl" {
         listen on $gateway  port https
         protocol "httpproxy"

         forward to <new-webserver>  port https
}
```

to automatic renew our certificates we need to speak to `daily(8)` (openbsd crontab)

```sh
## renew acme certificate every night
## edit crontab(8) to automate
~	*	*	*	*	acme-client example.com && rcctl reload httpd
```
as script

```sh
#!/bin/sh
acme-client example.com www.example.com
if [ $? -eq 0 ]
then
        rcctl reload httpd
fi
```
