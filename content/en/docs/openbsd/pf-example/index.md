---
title: "OpenBSD PF example"
description: "a pf.conf configuration example"
lead: "a pf.conf configuration example"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---


```sh
external_interface = "{vio0}"
wg_interface = "{wg0}"
allowed_ports_tcp = "{80,443}"
wg_port = "{8152}"
rate_limited_ports = "{3303,4404}"
prometheus_host = "{192.168.1.54}"
node_exporter = "{9100}"

#set skip on { lo wg }
set block-policy return
block log all

# pass wg0 peering
pass on $wg_interface

pass out quick from any to any

table <bruteforce> persist

# only block for 5 min, put
# */5     * * * *    pfctl -t bruteforce -T expire 300
# on roots crontab

block in log quick proto tcp from <bruteforce> to any port $rate_limited_ports

# == pass in inet6
#pass in on egress inet6 proto icmp6 all icmp6-type { echoreq routeradv neighbrsol neighbradv }

## == tcp input
pass in quick proto tcp from any to any port $allowed_ports_tcp

## == udp input
pass in quick on egress proto udp to port $wg_port

## == rate limit
pass log quick proto tcp from any to any port $rate_limited_ports flags S/SA keep state (max-src-conn 10, max-src-conn-rate 10/30, overload <bruteforce> flush global)
```
