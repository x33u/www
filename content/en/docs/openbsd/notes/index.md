---
title: "OpenBSD Notes"
description: "Common notes for OpenBSD"
lead: "Common notes for OpenBSD"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: false
---

##### configure `doas`
```sh
> echo 'permit username' > /etc/doas.conf
> echo 'permit nopass keepenv root as root' >> /etc/doas.conf
```

##### renew `Let's Encrypt` certificates
```sh
> acme-client x33u.org www.x33u.org
```

##### rescue system
```sh
> boot -s
> mount -uw /
> mount /usr # if seperatly mounted
> passwd # eg
> reboot
```


##### add floating ip for `Hetzner Cloud`
```sh
# edit "/etc/hostname.vio0"

# add floating ip by hand
inet alias 116.202.xxx.xxx 255.255.255.0

# add floating ip as command
> inet alias 116.202.xxx.xxx xfffffff

# enable config by running "netstart"
> sh /etc/netstart vio0
```


##### list disks
```sh
> sysctl hw.disknames
```

##### open encrypt disk
```sh
> bioctl -c C -l sd3a softraid0
```

##### serial c
```sh
> TERM=vt100
> export TERM
```

##### disable "reordering libraries"
```sh
> rcctl disable library_aslr
```

##### verify images
```sh
# get installXX.iso|img, openbsd-70-base.pub, SHA256 and SHA256.sig
# verify checksum on openbsd
> sha256 -C SHA256 
# verify checksum on linux
> sha256sum -c --ignore-missing SHA256
# verify signature
> signify -Cp openbsd-70-base.pub -x SHA256.sig install70.iso
```

##### upgrade
```sh
## download and verify latest bsd.rd to /
> signify -C -p /etc/signify/openbsd-68-base.pub -x SHA256.sig bsd.rd

## boot bsd.rd and choose (U)pgrade
## mount storage to /mnt if there is not enough space to upgrade
...
>> OpenBSD/amd64 BOOT 3.41
boot> bsd.rd
...

## after succesfull install reboot and run these commands
> syspatch
> pkg_add -Uu
> sysmerge -d
```

##### print partition table
```sh
> disklabel -E sd0
...
sd0> p g
OpenBSD area: 64-104856255; size: 50.0G; free: 0.0G
#                size           offset  fstype [fsize bsize   cpg]
  a:            10.0G               64  4.2BSD   2048 16384 12960 # /
  b:             2.0G         20971296    swap                    # none
  c:            50.0G                0  unused                    
  d:             3.0G         41961760  4.2BSD   2048 16384 12960 # /tmp
  e:             5.0G         25173856  4.2BSD   2048 16384 12960 # /usr
  f:             3.0G         35664288  4.2BSD   2048 16384 12960 # /home
  g:            27.0G         41961760  4.2BSD   2048 16384 12960 # /var
```

##### kvm installion vnc reverse shell
```sh
> ssh <host_ip> -L 5902:127.0.0.1:5902
```

##### change mirror
```sh
> edit /etc/installurl
```
