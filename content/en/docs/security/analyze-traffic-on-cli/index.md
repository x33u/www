---
title: "Analyze traffic on CLI by using TShark"
description: "short guide about how to use tshark"
lead: "short guide about how to use tshark"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---

To analyze the captured traffic we need some tools.<br>
I prefer the commandline interface and use the programm `tshark` for this.
<br>
<br>
<br>
to only read pcap files use
```sh
> tshark -r ethernet_traffic.pcap
```

use `display filter` (-Y) for wireshark filter syntax
```sh
## filter by ip adresses
> tshark -r ethernet_traffic.pcap -Y "ip.addr == 10.10.0.18"
## exclude port 80
> tshark -r ethernet_traffic.pcap -Y "tcp.dstport != 80"
## show only http and icmp traffic
> tshark -r ethernet_traffic.pcap -Y "tcp.port eq 80 or icmp"
## show only traffic in lan 10.10.x.x
> tshark -r ethernet_traffic.pcap -Y "ip.src==10.10.0.0/16 and ip.dst==10.10.0.0/16"
```
> NOTE: `and`, `or`, `()`, and `!` are used to combine statements<br>
for more filter examples see: [wireshark wiki](https://wiki.wireshark.org/DisplayFilters)

some nice oneliner
```sh
## pipe pcap file to awk to only show the captured ip addresses
> tshark -r ethernet_traffic.pcap  | \
awk '{ print gensub(/(.*)\..*/,"\\1","g",$3), $4, gensub(/(.*)\..*/,"\\1","g",$5) }'
## if there a lot of dupes append another awk pipe
> awk '!NF || !seen[$0]++'
```

