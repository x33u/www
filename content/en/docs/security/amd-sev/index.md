---
title: "AMD SEV - Secure Virtualisation"
description: "a how to make usage of AMD's SEV"
lead: "a how to make usage of AMD's SEV"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---


### Secure Encrypted Virtualization (SEV)

see: [https://developer.amd.com/sev](https://developer.amd.com/sev/)

```sh
> edit /etc/default/grub
>> GRUB_CMDLINE_LINUX=".... nomodeset rhgb quiet mem_encrypt=on kvm_amd.sev=1"

> grub2-mkconfig -o /boot/efi/EFI/almalinux/grub.cfg
>> Generating grub configuration file ...

> reboot

> cat /proc/cmdline
>> BOOT_IMAGE=(hd0,gpt2)/vmlinuz-4.18.0-305.12.1.el8_4.x86_64 root=/dev/mapper/almalinux-root ro crashkernel=auto resume=/dev/mapper/almalinux-swap rd.lvm.lv=almalinux/root rd.lvm.lv=almalinux/swap nomodeset rhgb quiet mem_encrypt=on kvm_amd.sev=1

> dnf install git-core
> git clone https://github.com/AMDESE/AMDSEV.git
> cd AMDSEV/distros/rhel-8.0
> ./build.sh
----
Last metadata expiration check: 2:53:23 ago on Wed 08 Sep 2021 03:31:19 PM CEST.
Package qemu-kvm-15:4.2.0-48.module_el8.4.0+2523+3300d70f.3.x86_64 is already installed.
Package qemu-img-15:4.2.0-48.module_el8.4.0+2523+3300d70f.3.x86_64 is already installed.
Package edk2-ovmf-20200602gitca407c7246bf-4.el8_4.1.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
cp ../launch-qemu.sh /usr/local/bin
----

> qemu-img create -f qcow2 rhel-8.qcow2 30G
> cp /usr/share/OVMF/OVMF_VARS.fd OVMF_VARS.fd
> launch-qemu.sh -hda rhel-8.qcow2 -cdrom /var/lib/libvirt/images/AlmaLinux-8.4-x86_64-boot.iso
----
char device redirected to /dev/pts/1 (label compat_monitor0)
qemu-kvm: sev_guest_init: Failed to open /dev/sev 'No such file or directory'
qemu-kvm: failed to initialize KVM: Operation not permitted
----
> memory encryption disabled?

> cat /sys/devices/virtual/dmi/id/board_{vendor,name,version}
ASUSTeK COMPUTER INC.
PRIME B450M-A
Rev X.0x

> dmesg|grep -i sme
[    0.001000] AMD Memory Encryption Features active: SME

> dmesg|grep -i sev
[    4.464266] SEV supported

> cat /sys/module/kvm_amd/parameters/sev
1

## -nosev works sadly
> launch-qemu.sh -hda rhel-8.qcow2 -cdrom /var/lib/libvirt/images/AlmaLinux-8.4-x86_64-boot.iso -nosev
```
