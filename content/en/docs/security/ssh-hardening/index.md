---
title: "SSH hardening example"
description: "how to hardening ssh service"
lead: "how to hardening ssh service"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "openbsd"
weight: 130
toc: true
---


_"OpenSSH is the premier connectivity tool for remote login with the SSH protocol. It encrypts all traffic to eliminate eavesdropping, connection hijacking, and other attacks. In addition, OpenSSH provides a large suite of secure tunneling capabilities, several authentication methods, and sophisticated configuration options."_

#### _goals:_

* setup sshd_conf
* audit with `ssh-audit`

<br>

re-generate `rsa` and `ed25519` keys
```sh
> rm /etc/ssh/ssh_host_*
> ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""
> ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""
```

remove small `diffie-hellmann` moduli
```sh
> awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.safe
> mv /etc/ssh/moduli.safe /etc/ssh/moduli
```

enable `rsa` and `ed25519` keys
```sh
> sed -i 's/^\#HostKey \/etc\/ssh\/ssh_host_\(rsa\|ed25519\)_key$/HostKey \/etc\/ssh\/ssh_host_\1_key/g' /etc/ssh/sshd_config
```

restrict supported key exchange, cipher, and MAC algorithms
```sh
> echo -e "\n# Restrict key exchange, cipher, and MAC algorithms, as per sshaudit.com\n# hardening guide.\nKexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256\nCiphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr\nMACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com\nHostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-512,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com" > /etc/ssh/sshd_config.d/ssh-audit_hardening.conf
```

restart sshd service
```sh
> systemctl restart sshd
```

<br>

### example sshd_conf configuration:
```sh
## use only version 2
Protocol 2

## specify ssh port
Port 22

## hostkey places
HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

## time to login
LoginGraceTime 60

## disable root login
PermitRootLogin no

## where authorized_keys are
AuthorizedKeysFile .ssh/authorized_keys

## pubkey authentication
PubkeyAuthentication yes

## disallow empty passwords
PermitEmptyPasswords no

## disallow passwords login
PasswordAuthentication no

## hide sensitiv information
##possible values are:DAEMON,	USER, AUTH, LOCAL0, LOCAL1, LOCAL2, LOCAL3, LOCAL4, LOCAL5, LOCAL6, LOCAL7
SyslogFacility AUTHPRIV

## loglevel
## possible values are:	QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2, and DEBUG3
LogLevel INFO

## allowed ssh login users
AllowUsers user1 user2 user3

## use only ipv4
AddressFamily inet

## max retries
MaxAuthTries 3

## check file modes and ownership of the user files
StrictModes yes

## specifies that .rhosts and .shosts files will not be used in HostbasedAuthentication
IgnoreRhosts yes

## disallow look up the remote host name
UseDNS no

## disallow HostbasedAuthentication
HostbasedAuthentication no

## disables cause of key authentication
ChallengeResponseAuthentication no

## only key authentication is allowed
AuthenticationMethods publickey

## symmetric algorithms
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes128-ctr

## message authentication codes
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com

## host key types
HostKeyAlgorithms ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa

## symmetric key exchange algorithms
KexAlgorithms curve25519-sha256@libssh.org

## disalow forwarding X11
X11Forwarding no

## print date and time of the last user login
PrintLastLog yes

## keep alive packages disabled
TCPKeepAlive no

## use kernel sandbox mechanisms
UsePrivilegeSeparation sandbox

## using PAM is allowed cause of forcing passwd change at first login
UsePAM yes

## disable message of the day
PrintMotd no

## print lawyer
Banner /etc/ssh/sshd-banner
```

##### example sshd-banner:
```sh
Warning!! Authorized users only.
```


<br>

check sshd_conf with `ssh-audit`
```sh
## == clone jtesta fork of ssh-audit
> git clone https://github.com/jtesta/ssh-audit
> cd ssh-audit
> ./ssh-audit.py 192.168.1.22 -p2222
```


> check git repo for more informations: [jtesta/ssh-audit](https://github.com/jtesta/ssh-audit)

> read here why `ssh-rsa` sucks: [practical SHA-1 collisions](https://eprint.iacr.org/2020/014.pdf)

