---
title : "Yet Another Website"
description: "x33u is a Hugo theme for building secure, fast, and SEO-ready documentation websites, which you can easily update and customize."
lead: "... from earth. This website serves as a platform for my thoughts, as a support for the many projects and especially as a playground."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---

