---
title : "Datenschutz"
description: "Privacy Policy"
lead: ""
date: 2020-10-06T08:48:45+00:00
lastmod: 2020-10-06T08:48:45+00:00
draft: false
images: []
weight: 100
---

### Datenschutzerklärung

<font color="">

Im Folgenden finden Sie Informationen zu Art, Umgang und Zweck personenbezogener Daten innerhalb dieser Onlinepräsenz.

</font>


#### Verantwortlicher

Gordon Lenz<br>
Westring 7<br>
39108 Magdeburg<br>
Germany<br>
Telefon: <font color="#68696f"> +49 (0) 391 / 59 848 746</font><br>
Fax: <font color="#68696f"> +49 (0) 391 / 59 848 773</font><br>
E-Mailadresse: <font color="#68696f">domain(at)x33u.org</font> <br>
PGP-Key: <font color="#68696f" text-decoration="none">[0x2E0E13A083979D6E](https://www.x33u.org/dwnld/webmaster_x33u_pub.asc)</font> <br>
Fingerprint: <font color="#68696f">
CF30 9589 0CDD 05AC B4CD 8C7B 2E0E 13A0 8397 9D6E
</font> <br><br>
Link zum Impressum: <font color="#68696f">https://www.x33u.org/impressum/</font>

#### Welche Daten werden erhoben?

Systembedingt übermittelt Ihr Internet-Browser folgende Kommunikationsdaten:

Die angesurfte Domain: <br>
<font color="#68696f">z.B.  _"www.x33u.org"_ </font>

Ihre IP-Adresse: <br>
<font color="#68696f">z.B.  _"192.168.2.1"_ </font>

Der genaue Zeitpunkt des Zugriffs: <br>
<font color="#68696f">z.B.  _"23.03.2019 - 20:31Uhr"_ </font>

Welche Art von HTTP-Request aufgerufen wurde: <br>
<font color="#68696f">z.B.  _"GET"_ </font>

Die aufgerufende URL: <br>
<font color="#68696f">z.B.  _"/datenschutz"_ </font>

Den HTTP-Statuscode: <br>
<font color="#68696f">z.B.  _"200"_ </font>

Der verwendete Browser, sowie das Betriebssystem <br>
<font color="#68696f">z.B.  _"Mozilla/5.0 (X11; Linux_ x86_64; rv:67.0) Gecko/20100101 Firefox/67.0"_ </font>


#### Was sind personenbezogene Daten?

Neben Daten wie Name, Adresse, Wohnort und Geschlecht zählen auch folgende Angaben zu personenbezoge Daten:

IP-Adressen

Mobilfunkrufnummern

Standortdaten

Fotos, Videos, Audiodatein

Nutzungsdaten

Kalendereinträge

Registrierungsdaten

Nachrichten

Kontoverbindungsdaten

#### Welche personenbezogene Daten werden erhoben?

Standardmäßig fallen bei dem Aufruf einer Webseite im Internet sogenannte Meta-/Kommunikationsdaten an. Geräte-Informationen, referrer, IP-Adresse etc. werden an den Webserver übermittelt. Diese standardmäßig anfallenden Daten werden erhoben. Dabei werden die personenbezogenen Daten (IP-Adressen) direkt von dem Webserver anonymisiert gespeichert. <br>

<font color="#68696f">

z.B. _"192.168.2.0"_

</font>

#### Was passiert mit den erhobenen Daten?

<font color="">

Die erhobenen Daten werden allesamt nach 24 Stunden automatisch per <a href="https://man.openbsd.org/newsyslog.8" target="blank">newsyslog(8)</a> gelöscht.

</font>

#### Warum werden diese Daten überhaupt erhoben?

<font color="">

Die sogenannten *Meta-/Kommunikationsdaten* sind technisch für den Aufbau einer Verbindung von einem Internetbrowser zu einem Webserver erforderlich.

</font>

#### Werden die erhobenenen Daten an Dritte weitergegeben?

<font color="">

Es erfolgt keinerlei Weitergabe an Dritte, auch nicht über Seitenkanäle wie die externe Einbindung von Schriftarten oder Werbetracking. Eine Überprüfung dieser Angabe kann über die Website des Projekts
<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fx33u.org" target="blank">Webbkoll von dataskydd</a> erfolgen.

</font>

#### Findet ein grenzübergreifender Datenverkehr außerhalb der EU statt?

<font color="">

Nein, alle Daten die auf dieser Seite erhoben werden bleiben bis zur automatischen Löschung auf diesem Server, mit Standort in den Niederlanden.

</font>

#### Welche Maßnahmen werden ergriffen, um die Sicherheit der Daten zu gewährleisten?

<font color="">

Es findet eine erzwungene Übetragungsverschlüsselung statt.

</font>
